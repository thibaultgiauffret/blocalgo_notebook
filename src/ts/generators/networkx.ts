import * as Blockly from "blockly";
import { pythonGenerator } from "blockly/python";

export function setNetworkxGen() {
    pythonGenerator["networkx_graph"] = function (block: any) {
        var variable_var = pythonGenerator.variableDB_.getName(
            block.getFieldValue("variable"),
            Blockly.Names.NameType.VARIABLE
        );
        var code = variable_var + " = nx.Graph()\r\n";
        return code;
    }

    pythonGenerator["networkx_draw"] = function (block: any) {
        var variable_var = pythonGenerator.variableDB_.getName(
            block.getFieldValue("variable"),
            Blockly.Names.NameType.VARIABLE
        );
        var value = pythonGenerator.valueToCode(
            block,
            "with_label",
            pythonGenerator.ORDER_NONE
        );
        var code = "nx.draw(" + variable_var + ", with_labels=" + value + ")\r\n";
        return code;
    }

    pythonGenerator["networkx_add_edge"] = function (block: any) {
        var variable_var = pythonGenerator.variableDB_.getName(
            block.getFieldValue("variable"),
            Blockly.Names.NameType.VARIABLE
        );
        var value_list_value = block.getFieldValue("add_edge_value");
        var value_list_set = block.getFieldValue("add_edge_value2");
        var code = variable_var + ".add_edge('" + value_list_value + "', '" + value_list_set + "')\r";
        return code;
    }

    pythonGenerator["networkx_add_node"] = function (block: any) {
        var variable_var = pythonGenerator.variableDB_.getName(
            block.getFieldValue("variable"),
            Blockly.Names.NameType.VARIABLE
        );
        var value_list_value = block.getFieldValue("add_node_value");
        var code = variable_var + ".add_node('" + value_list_value + "')\r";
        return code;
    }

    pythonGenerator["networkx_number_of_nodes"] = function (block: any) {
        var variable_var = pythonGenerator.variableDB_.getName(
            block.getFieldValue("variable"),
            Blockly.Names.NameType.VARIABLE
        );
        var code = variable_var + ".number_of_nodes()";
        return [code, pythonGenerator.ORDER_NONE];
    }

    pythonGenerator["networkx_number_of_edges"] = function (block: any) {
        var variable_var = pythonGenerator.variableDB_.getName(
            block.getFieldValue("variable"),
            Blockly.Names.NameType.VARIABLE
        );
        var code = variable_var + ".number_of_edges()";
        return [code, pythonGenerator.ORDER_NONE];
    }

    pythonGenerator["networkx_diameter"] = function (block: any) {
        var variable_var = pythonGenerator.variableDB_.getName(
            block.getFieldValue("variable"),
            Blockly.Names.NameType.VARIABLE
        );
        var code = "nx.diameter(" + variable_var + ")";
        return [code, pythonGenerator.ORDER_NONE];
    }

    pythonGenerator["networkx_radius"] = function (block: any) {
        var variable_var = pythonGenerator.variableDB_.getName(
            block.getFieldValue("variable"),
            Blockly.Names.NameType.VARIABLE
        );
        var code = "nx.radius(" + variable_var + ")";
        return [code, pythonGenerator.ORDER_NONE];
    }

    pythonGenerator["networkx_center"] = function (block: any) {
        var variable_var = pythonGenerator.variableDB_.getName(
            block.getFieldValue("variable"),
            Blockly.Names.NameType.VARIABLE
        );
        var code = "nx.center(" + variable_var + ")";
        return [code, pythonGenerator.ORDER_NONE];
        }

}