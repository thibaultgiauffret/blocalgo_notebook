# BlocAlgo Notebook
A bloc to script online app to learn programming languages with Jupyter Notebook. 

**DISCLAMER : The development is still in progress and the only usable language is Python. This alpha version is not suitable for production use yet.**

This is a fork of Basthon Notebook made by Romain Casati (https://framagit.org/basthon/basthon-notebook) combined with the BlocAlgo project (https://framagit.org/ThibGiauffret/BlocAlgo). 

Live demo : https://www.ensciences.fr/addons/blocalgo_notebook/

*Code is not commented yet. Coming soon...*

## Installation
See [Basthon-Kernel's Readme](https://framagit.org/casatir/basthon-kernel/-/blob/master/README.md#basthon).

    npm install
    npm start
